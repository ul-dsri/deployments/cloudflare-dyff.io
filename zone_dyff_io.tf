# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "cloudflare_zone" "dyff_io" {
  account_id = local.cloudflare_account_id
  zone       = "dyff.io"
}

resource "cloudflare_zone_settings_override" "dyff_io" {
  zone_id = cloudflare_zone.dyff_io.id
  settings {
    always_use_https         = "on"
    automatic_https_rewrites = "on"
    brotli                   = "on"
    browser_check            = "on"
    cache_level              = "aggressive"
    development_mode         = "off"
    early_hints              = "on"
    email_obfuscation        = "on"
    min_tls_version          = "1.2"
    opportunistic_encryption = "on"
    rocket_loader            = "on"
    security_level           = "medium"
    ssl                      = "strict"
    tls_1_3                  = "on"
    universal_ssl            = "on"

    security_header {
      enabled = true
    }
  }
}

# domain

# It's normally not possible to have a CNAME for an apex domain, but Cloudflare
# supports CNAME flattening for this purpose:
# https://developers.cloudflare.com/dns/cname-flattening/
resource "cloudflare_record" "docs_dyff_io_cname" {
  zone_id = cloudflare_zone.dyff_io.id
  type    = "CNAME"
  name    = "docs.dyff.io"
  content = "dyff.gitlab.io"
  proxied = true
}

resource "cloudflare_record" "dyff_io_cname" {
  zone_id = cloudflare_zone.dyff_io.id
  type    = "CNAME"
  name    = "dyff.io"
  content = "dyff.gitlab.io"
  proxied = true
}

resource "cloudflare_record" "api_dyff_io_a" {
  zone_id = cloudflare_zone.dyff_io.id
  type    = "A"
  name    = "api.dyff.io"
  content = "34.120.199.41"
  proxied = true
}

# domain verification

resource "cloudflare_record" "dyff_io_github" {
  zone_id = cloudflare_zone.dyff_io.id
  type    = "TXT"
  name    = "_github-challenge-dyff-io-org"
  content = "7027b46437"
}

resource "cloudflare_record" "dyff_io_gitlab" {
  zone_id = cloudflare_zone.dyff_io.id
  type    = "TXT"
  name    = "_gitlab-pages-verification-code"
  content = "gitlab-pages-verification-code=34430e6070da566a7c067eece1c7340b" # pragma: allowlist secret
}

resource "cloudflare_record" "docs_dyff_io_gitlab" {
  zone_id = cloudflare_zone.dyff_io.id
  type    = "TXT"
  name    = "_gitlab-pages-verification-code.docs"
  content = "gitlab-pages-verification-code=47d35a252384635a8cfe43320a347bc8" # pragma: allowlist secret
}

resource "cloudflare_record" "dyff_io_google" {
  zone_id = cloudflare_zone.dyff_io.id
  type    = "TXT"
  name    = "dyff.io"
  content = "google-site-verification=_lOokfYyfUvZYaqAxW91WUz4_C3LVNlO573UDa3iKv8" # pragma: allowlist secret
}

resource "cloudflare_record" "dyff_io_bing" {
  zone_id = cloudflare_zone.dyff_io.id
  type    = "CNAME"
  name    = "872ef7d11c50811808cbb5fb6d3a2a63" # pragma: allowlist secret
  content = "verify.bing.com"
}

# email security

resource "cloudflare_record" "dyff_io_spf" {
  zone_id = cloudflare_zone.dyff_io.id
  type    = "TXT"
  name    = "dyff.io"
  content = "v=spf1 -all"
}

resource "cloudflare_record" "dyff_io_dmarc" {
  zone_id = cloudflare_zone.dyff_io.id
  type    = "TXT"
  name    = "_dmarc"
  content = "v=DMARC1; p=reject; sp=reject; adkim=s; aspf=s;"
}

resource "cloudflare_record" "dyff_io_dkim" {
  zone_id = cloudflare_zone.dyff_io.id
  type    = "TXT"
  name    = "*._domainkey"
  content = "v=DKIM1; p="
}
