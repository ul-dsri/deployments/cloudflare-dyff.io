# cloudflare-zone-dyff deployment

<!-- BADGIE TIME -->

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Cloudflare resources to manage the [dyff.io](https://dyff.io) domain.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## About this project

This project contains [Terraform](https://www.terraform.io/) code that describes
our implementation of [Cloudflare](https://www.cloudflare.com/) at DSRI. It is
made available with the permissive [Apache License 2.0](LICENSE) so that you may
study and learn from our workflows and practices.

## About us

The Digital Safety Research Institute at UL Research Institutes is an institute
committed to making the digital ecosystem safer.

As part of our commitment, we strive to make our [Infrastructure as
Code](https://en.wikipedia.org/wiki/Infrastructure_as_code) (IaC) deployments
available as open source projects. In doing so, we promote open discussion,
knowledge sharing, and continuous improvement of our infrastructure practices
via crowd-sourced peer review.

<!-- prettier-ignore-start -->
<!-- prettier-ignore-end -->
