# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

moved {
  from = cloudflare_record.api_dyff_io_a
  to   = module.root.cloudflare_record.api_dyff_io_a
}
moved {
  from = cloudflare_record.docs_dyff_io_cname
  to   = module.root.cloudflare_record.docs_dyff_io_cname
}
moved {
  from = cloudflare_record.docs_dyff_io_gitlab
  to   = module.root.cloudflare_record.docs_dyff_io_gitlab
}
moved {
  from = cloudflare_record.dyff_io_bing
  to   = module.root.cloudflare_record.dyff_io_bing
}
moved {
  from = cloudflare_record.dyff_io_cname
  to   = module.root.cloudflare_record.dyff_io_cname
}
moved {
  from = cloudflare_record.dyff_io_dkim
  to   = module.root.cloudflare_record.dyff_io_dkim
}
moved {
  from = cloudflare_record.dyff_io_dmarc
  to   = module.root.cloudflare_record.dyff_io_dmarc
}
moved {
  from = cloudflare_record.dyff_io_github
  to   = module.root.cloudflare_record.dyff_io_github
}
moved {
  from = cloudflare_record.dyff_io_gitlab
  to   = module.root.cloudflare_record.dyff_io_gitlab
}
moved {
  from = cloudflare_record.dyff_io_google
  to   = module.root.cloudflare_record.dyff_io_google
}
moved {
  from = cloudflare_record.dyff_io_spf
  to   = module.root.cloudflare_record.dyff_io_spf
}
moved {
  from = cloudflare_zone.dyff_io
  to   = module.root.cloudflare_zone.dyff_io
}
moved {
  from = cloudflare_zone_settings_override.dyff_io
  to   = module.root.cloudflare_zone_settings_override.dyff_io
}
